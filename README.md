This is a sample project with a (partial) workaround for PyCharm issue *[PY-44557](https://youtrack.jetbrains.com/issue/PY-44557)*

Main "solution" is to create separate python file (fsa/models/abstract.py) and a "stub" file for it.
Real models should be derived from fsa.models.abstract.AbstractBaseModel for this workaround to work.
Example:

```python
from fsa.addons import db
from fsa.models.abstract import AbstractBaseModel


class Items(AbstractBaseModel):
    __tablename__ = 'items'
    
    item_id = db.Column(db.Integer, primary_key=True)
    item_name = db.Column(db.String)
```