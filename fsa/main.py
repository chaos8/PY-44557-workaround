# coding=utf-8

from __future__ import print_function

import logging

from flask import Flask

from fsa.addons import db
from fsa.models.user import User, UserRole

logger = logging.getLogger(__name__)


def create_app():
    logger.info('initializing flask application')
    flask_app = Flask(__name__)
    logger.info('initializing flask-sqlalchemy application')
    db.init_app(flask_app)
    with flask_app.app_context():
        test_run()

    from fsa.views import mainview
    flask_app.register_blueprint(mainview)
    return flask_app


def test_run():
    logger.info('creating database table and what not...')
    db.create_all()
    print(User.__table__.columns.keys())
    print(User.__mapper__.columns.keys())
    print(User.query)
    print(User.user_id)
    print(User.user_id.label('foo'))
    print(User.user_id.isnot(0))
    print(User.user_id.desc())

    u = User(
        name='foo',
    )
    role1 = UserRole(
        name='admin',
    )
    role1.user = u
    role2 = UserRole(
        name='author',
    )
    role2.user = u
    db.session.add(u)
    db.session.add(role1)
    db.session.add(role2)
    db.session.commit()

    print('CREATED USER', u, u.roles)

    for role in u.roles:
        print('ROLE', role.name)


if __name__ == '__main__':
    app = create_app()

    with app.app_context():
        test_run()
