from flask import Blueprint, Response

from fsa.models.user import User

mainview = Blueprint('mainview', __name__)


@mainview.route('/')
def main_index():
    users = User.query.all()

    response = Response('\n'.join(str(u) for u in users))
    response.headers['Content-Type'] = 'text/plain'

    return response
