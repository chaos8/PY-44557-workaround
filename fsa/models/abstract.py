from fsa.addons import db


class AbstractBaseModel(db.Model):
    __abstract__ = True
