from uuid import uuid4

from fsa.addons import db
from fsa.models.abstract import AbstractBaseModel


class User(AbstractBaseModel):
    __tablename__ = 'users'

    user_id = db.Column(db.String, primary_key=True, default=lambda: str(uuid4()))
    name = db.Column(db.String, primary_key=True)

    roles = db.relationship(lambda: UserRole, back_populates='user')


class UserRole(AbstractBaseModel):
    __tablename__ = 'user_roles'

    role_id = db.Column(db.String, primary_key=True, default=lambda: str(uuid4()))
    name = db.Column(db.String)
    user_id = db.Column(db.String, db.ForeignKey(User.user_id))

    user = db.relationship(User, back_populates='roles')
